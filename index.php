<?php
	ini_set('display_errors', 1);
	error_reporting(E_ALL);
	
	require_once 'lib/' . 'PersistModelAbstract.php';
	require_once 'lib/' . 'Application.php';
	
	require_once 'config/' . 'app.config.php';
	
	setlocale(LC_ALL, 'pt_BR');
	
	$o_Application = new Application();
	$o_Application->dispatch();
?>