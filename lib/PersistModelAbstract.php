<?php

abstract class PersistModelAbstract {

	/**
	* Variavel responsavel por guardar dados da conexao do banco
	*/
	protected static $_db;
	protected static $_dbConfig;
	protected static $_errors;
	private static $_finalized;

	public static function getDB() {
		self::initialize();
		return self::$_db;
	}

	public function __construct() {

	}

	public static function initialize() { 
		self::setDbConfig();
		$dsn = 'mysql:host=' . self::$_dbConfig['host'] . ';dbname=' . self::$_dbConfig['banco'];
		if (!self::$_db) {
			self::$_db = new PDO($dsn, self::$_dbConfig['usuario'], self::$_dbConfig['senha'], array(PDO::ATTR_PERSISTENT => false));
			self::$_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			self::$_db->exec("set names utf8");
			self::$_db->beginTransaction();
		}
		self::closeSleepingConnections();
	}
	
	public static function closeSleepingConnections() {
		$db = self::$_db;
		$sql = "SHOW FULL PROCESSLIST";
		$query = $db->prepare($sql);
		$query->execute();	
		if($query->rowCount() > 20) {
			while ($linha = $query->fetchObject()) {
				if($linha->Time > 200) {
					$process = $linha->Id;
					$kill = $db->prepare("KILL $process");
					$kill->execute();
				}
			}
		}
		self::finalize();
		$db = null;
	}

	public static function getDbConfig() {
		return self::$_dbConfig;
	}

	public static function setDbConfig() {
		self::$_dbConfig = self::$_dbConfig ? self::$_dbConfig : require ('config/db.config.php');
	}

	public static function getErrors() {
		return self::$_errors;
	}

	public static function addError($error) {
		self::$_errors[] = $error;
	}

	public static function finalize() {
		if (self::$_finalized) {
			return;
		}
		self::$_finalized = true;
		if (self::getErrors()) {
			self::$_db->rollback();
		} else {
			self::$_db->commit();
		}
	}

}