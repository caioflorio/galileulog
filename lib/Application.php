<?php
/**
* Essa funcao garante que todas as classes 
* da pasta lib serao carregadas automaticamente
*/
function __autoload($class)
{
	$file = "lib/".$class.".php";
	
	if (file_exists($file))
		require_once $file;
}


/** 
* Verifica qual classe controlador (Controller) o usuario deseja chamar
* e qual metodo dessa classe (Action) deseja executar
* Caso o controlador (controller) nao seja especificado, o IndexControllers sera o padrao
* Caso o metodo (Action) nao seja especificado, o indexAction sera o padrao
**/
class Application
{
	/**
	* Usada pra guardar o nome da classe
	* de controle (Controller) a ser executada
	*/
	protected $controller;
	
	/**
	* Usada para guardar o nome do metodo da
	* classe de controle (Controller) que devera ser executado
	*/
	protected $action;
	
	function __construct()
	{
		date_default_timezone_set('America/Sao_Paulo');
	}
	
	/**
	* Verifica se os parametros de controlador (Controller) e acao (Action) foram
	* passados via parametros "Post" ou "Get" e carrega tais dados
	* nos respectivos atributos da classe
	*/
	private function loadRoute()
	{
		/*
		* Se o controller nao for passado,
		* assume-se como padrao o controller 'IndexController';
		*/
		if (isset($_REQUEST['controller']))
			$this->controller = $_REQUEST['controller'];
		elseif (isset($_REQUEST['controle']))
			$this->controller = $_REQUEST['controle'];
		else 
			$this->controller = 'index';

		/*
		* Se a action nao for passada,
		* assume-se como padrao a action 'IndexAction';
		*/
		if (isset($_REQUEST['action']))
			$this->action = $_REQUEST['action'];
		elseif (isset($_REQUEST['acao']))
			$this->action = $_REQUEST['acao'];
		else 
			$this->action = 'index';
	}
	
	/**
	* 
	* Instancia classe referente ao Controlador (Controller) e executa
	* metodo referente e acao (Action)
	* @throws Exception
	*/
	public function dispatch()
	{
		$this->loadRoute();
		
		//verificando se o arquivo de controle existe
		$controller_file = "controllers/".ucfirst($this->controller)."Controller.php";
		
		if (file_exists($controller_file))
			require_once $controller_file;
		else
			throw new Exception('Arquivo '.$controller_file.' nao encontrado');

		//verificando se a classe existe
		$class = $this->controller.'Controller';
		
		if (class_exists($class))
			$object = new $class;
		else
			throw new Exception("Classe '.$class' nao existe no arquivo '.$controller_file'");

		//verificando se o metodo existe
		$method = $this->action.'Action';
		
		if (method_exists($object,$method))
			$object->$method();
		else
			throw new Exception("Metodo '.$method' nao existe na classe '.$class'");	
	}
	
	/**
	 * Obter a URL do site
	 */
	public static function getHome()
	{
		$site = $_SERVER ['REQUEST_URI'];
		$site = explode('?',$site);
		$site = $site[0];
		$site = explode('/',$site);

		$home = "http://".$_SERVER['SERVER_NAME'];

		for ($i = 0; $i < count($site) - 1; $i++)
			$home .= strlen($site[$i]) > 0 ? "/".$site[$i] : "";

		$home .= "/";

		return $home;
	}
	
	/**
	* Redireciona a chamada http para outra pagina
	*/
	public static function redirect( $uri )
	{
		header("Location: $uri");
	}
	
	/**
	* Redireciona a chamada http para outra pagina mantendo os parametros POST
	*/
	public static function persistenceRedirect( $uri )
	{
		header("Location: $uri", true, 307);
	}
}
?>