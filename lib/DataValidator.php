<?php
/**
 * Classe designada a validacao de formato de dados
 */
class DataValidator
{
	/**
	 * Verifica se o dado passado esta vazio
	 * @param mixed $mx_value
	 * @return boolean
	 */
	static function isEmpty( $value )
	{
		return 	!isset($value) or
				is_null($value) or
				(is_string($value) and strlen($value) == 0) or
				(is_numeric($value) and $value <= 0) or
				(is_array($value) and count($value) == 0) or
				(is_bool($value) && $value == false )
				;
	}

	/**
	 * Verifica se o dado passado e um numero
	 * @param mixed $mx_value;
	 * @return boolean
	 */
	static function isNumeric( $value )
	{
		if (!isset($value) or is_null($value))
			return false;
		
		$value = str_replace(',', '.', $value);
		
		if (!is_numeric($value))
			return false;
		
		return true;
	}	
	
}
?>