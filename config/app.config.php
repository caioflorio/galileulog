<?php

function getSite() {
    $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"], 0, strpos($_SERVER["SERVER_PROTOCOL"], '/'))) . '://';
    //$domainName = $_SERVER['HTTP_HOST'] . '/galileuLog/';
    $domainName = "galileulog.local/";
    
    return $protocol . $domainName;
}

define('SITE_URL', getSite());
define('ABSOLUTE_PATH', dirname(__DIR__));
chdir(ABSOLUTE_PATH);
setlocale(LC_ALL, 'pt_BR');