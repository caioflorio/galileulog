GlobalUI = {
	init: function () {
		this.bindUIActions();
	},

	bindUIActions: function () {


		// Marcando/Desmarcando checkbox
		$('.toggle').on('toggle', function(e, active) {

			var $toogCheck = $(this).prev('.check-toggle');

			if( active ) {
				$toogCheck.prop('checked', true);
			} else {  
				$toogCheck.prop('checked', false);
			} 
			$toogCheck.trigger("change");

		});


		/* -----------------------------------
		 SHOW CHIDS ON SELECT	
		 -------------------------------------- */
		 $(document).on('change', '[data-js="sel-has-child"]', function () {

		 	var opcao = $(this).find('option:selected');
		 	var $parent = $(this).closest('.form-group');
		 	var $childFields = $parent.next('.child-fields');

		 	if (opcao.hasClass('trigger-childs')) {
			// mostra campos filhos
			$childFields.slideDown(100);
			} else {
				// esconde campos filhos
				$childFields.slideUp(100);
			}
		});

		$('[data-js="sel-has-child"]').trigger('change');
		// ---------------------------------------


		/* ----------------------------------
		 SIDEBAR 
		 ------------------------------------- */
		$('.nav-quirk .with-sub').bind('click', function () {
			$(this).find('.children').slideToggle('fast');
		});

		// ----------------------------------------------------

		/* -----------------------------------
		 ABAS 
		 -------------------------------------- */

		// Preenche o hidden de numero de aba
		// com o numero da aba clicada ao clicar
		$('.nav-tabs li').bind('click', function () {
			var tabNum = $(this).index() + 1;
			$('.numero-aba').val(tabNum);
		});

		// Ao carregar a página, verifica o valor do hidden de numero de aba
		// e seleciona a aba de acordo com esse número.
		//$('.main-tabs-nav li:eq(' + (parseInt($('.numero-aba').val()) - 1) + ') a').tab('show');
		$('.nav-tabs li:eq(' + (parseInt($('.numero-aba').val()) - 1) + ') a').tab('show');

		/// ABAS ------------------------------------------------


		// Custom Select
		$('.select2').select2();

	}
}