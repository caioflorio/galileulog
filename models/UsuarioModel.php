<?php

require_once(ABSOLUTE_PATH . '/lib/PersistModelAbstract.php');
require_once(ABSOLUTE_PATH . '/classes/Usuario.class.php');

class UsuarioModel extends PersistModelAbstract {

	/**
	 * Efetua o login do usuario.
	 *
	 * @param Usuario $usuario
	 * @return Usuario
	 */
	public static function login($usuario) {

		if (DataValidator::isEmpty($usuario->getEmail()))
			throw new Exception('O campo e-mail é obrigatório.');

		if (!filter_var($usuario->getEmail(), FILTER_VALIDATE_EMAIL))
			throw new Exception('E-mail inválido.');

		if (DataValidator::isEmpty($usuario->getSenha()))
			throw new Exception('O campo senha é obrigatório.');

		$user = self::getByLogin($usuario);

		if (DataValidator::isEmpty($user))
			throw new Exception('Usuário não identificado.');

		if ($user->getSenha() != md5($usuario->getSenha()))
			throw new Exception('Senha não confere.');

		return $user;
	}

	/**
	 * Retorna o usuario identificado pelo login.
	 *
	 * @param Usuario $usuario
	 * @param object $db
	 * @return Usuario
	 */
	public static function getByLogin($usuario) {

		$db = self::getDB();

		$user = null;

		if (!DataValidator::isEmpty($usuario->getEmail())) {

			$usuarios = self::lista(null, null, $usuario->getEmail(), 'A');

			if (!DataValidator::isEmpty($usuarios['usuarios'])) {
				$user = $usuarios['usuarios'][0];
			}
		}

		$db = null;

		return $user;
	}

	/**
	 * Retorna usuário cadastrado com o ID passado
	 */
	public static function getById($id) {

		$db = self::getDB();

		if (DataValidator::isEmpty($id)) {
			return null;
		}

		$sql = " SELECT * FROM tb_usuarios WHERE id = :id LIMIT 1";

		$query = $db->prepare($sql);

		$query->bindValue(':id', $id, PDO::PARAM_INT);

		$query->execute();

		$linha = $query->fetchObject();
		
		if(!$linha) return null;

		$usuario = new Usuario();
		$usuario->setId($linha->id);
		$usuario->setNome($linha->nome);
		$usuario->setEmail($linha->email);
		$usuario->setSenha($linha->senha);
		$usuario->setStatus($linha->status);
		$usuario->setDataCriacao(date_create($linha->data_criacao));
		$usuario->setDataAtualizacao(date_create($linha->data_atualizacao));

		$db = null;

		return $usuario;
	}

	/**
	 * Retorna lista de usuarios 
	 * @param int $usuario_id
	 * @param string $usuario_nome
	 * @param string $usuario_email
	 * @param int $usuario_status
	 * @return Array com lista de usuarios e total de linhas
	 */
	public static function lista($usuario_id = null, $usuario_nome = null, $usuario_email = null, $usuario_status = null) {

		$db = self::getDB();

		$sql = "SELECT * FROM tb_usuarios WHERE 1=1";

		if (!DataValidator::isEmpty($usuario_id)) {
			$sql .= " AND id = :id ";
		}
		if (!DataValidator::isEmpty($usuario_nome)) {
			$sql .= " AND nome LIKE :nome ";
		}
		if (!DataValidator::isEmpty($usuario_email)) {
			$sql .= " AND email LIKE :email ";
		}
		if (!DataValidator::isEmpty($usuario_status)) {
			$sql .= " AND status = :status ";
		}

		$sql .= " ORDER BY nome ASC";

		$query = $db->prepare($sql);

		if (!DataValidator::isEmpty($usuario_id)) {
			$query->bindValue(':id', $usuario_id, PDO::PARAM_INT);
		}

		if (!DataValidator::isEmpty($usuario_nome)) {
			$query->bindValue(':nome', "%$usuario_nome%", PDO::PARAM_STR);
		}

		if (!DataValidator::isEmpty($usuario_email)) {
			$query->bindValue(':email', "%$usuario_email%", PDO::PARAM_STR);
		}

		if (!DataValidator::isEmpty($usuario_status)) {
			$query->bindValue(':status', $usuario_status, PDO::PARAM_INT);
		}

		$query->execute();

		$usuarios = array();

		while ($linha = $query->fetchObject()) {
			$usuario = new Usuario();
			$usuario->setId($linha->id);
			$usuario->setNome($linha->nome);
			$usuario->setEmail($linha->email);
			$usuario->setSenha($linha->senha);
			$usuario->setStatus($linha->status);
			$usuario->setDataCriacao(date_create($linha->data_criacao));
			$usuario->setDataAtualizacao(date_create($linha->data_atualizacao));

			$usuarios[] = $usuario;
		}

		$total_linhas = count($usuarios);

		$db = null;

		return array('usuarios' => $usuarios, 'total_linhas' => $total_linhas);
	}

	/**
	 * Cria novo ou atualiza registro no banco.
	 * Pede um objeto da classe Usuario
	 * Retorna ID do objeto
	 */
	public static function salvar(Usuario $usuario) {

		$db = self::getDB();

		//Criar novo registro
		if (DataValidator::isEmpty($usuario->getId())) {

			$sql = "INSERT INTO tb_usuarios (
				nome,
				email,
				senha,
				status,
				data_criacao,
				data_atualizacao
			)VALUES(
				:nome,
				:email,
				:senha,
				:status,
				NOW(),
				NOW()
			)";

			$query = $db->prepare($sql);

			$query->bindValue(':nome', $usuario->getNome(), PDO::PARAM_STR);
			$query->bindValue(':email', $usuario->getEmail(), PDO::PARAM_STR);
			$query->bindValue(':senha', $usuario->getSenha(), PDO::PARAM_STR);
			$query->bindValue(':status', $usuario->getStatus(), PDO::PARAM_INT);

			$query->execute();

			$retorno = $db->lastInsertId();

			self::finalize();

			$db = null;

			return $retorno;

		//Editar registro existente
		} else {

			$sqlSenha = "";
			if (!DataValidator::isEmpty($usuario->getSenha())) {
				$sqlSenha = " senha = :senha, ";
			}

			$sql = "UPDATE tb_usuarios SET
			nome = :nome,
			email = :email,
			" . $sqlSenha . "
			status = :status,
			data_atualizacao = NOW()

			WHERE id = :id";

			$query = $db->prepare($sql);

			$query->bindValue(':nome', $usuario->getNome(), PDO::PARAM_STR);
			$query->bindValue(':email', $usuario->getEmail(), PDO::PARAM_STR);
			if (!DataValidator::isEmpty($usuario->getSenha())) {
				$query->bindValue(':senha', $usuario->getSenha(), PDO::PARAM_STR);
			}
			$query->bindValue(':status', $usuario->getStatus(), PDO::PARAM_INT);
			$query->bindValue(':id', $usuario->getId(), PDO::PARAM_INT);

			$query->execute();

			self::finalize();

			$db = null;

			return $usuario->getId();
		}
	}

}
