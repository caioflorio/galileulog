<?php
$adm = false;
?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<!--<link rel="shortcut icon" href="<?php echo SITE_URL; ?>assets/img/favicon.png" type="image/png">-->

	<title>Sistema GalileuLog - por Caio Florio</title>

	<link rel="stylesheet" href="<?php echo SITE_URL; ?>assets/lib/fontawesome/css/font-awesome.css">
	<link rel="stylesheet" href="<?php echo SITE_URL; ?>assets/lib/jquery-toggles/toggles-full.css">
	<link rel="stylesheet" href="<?php echo SITE_URL; ?>assets/lib/select2/select2.css">

	<link rel="stylesheet" href="<?php echo SITE_URL; ?>assets/css/quirk.css">
	<link rel="stylesheet" href="<?php echo SITE_URL; ?>assets/css/estilo.css">

	<script src="<?php echo SITE_URL; ?>assets/lib/modernizr/modernizr.js"></script>

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="<?php echo SITE_URL; ?>assets/lib/html5shiv/html5shiv.js"></script>
	<script src="<?php echo SITE_URL; ?>assets/lib/respond/respond.src.js"></script>
	<![endif]-->

	</head>

	<body>

	<header>
		<div class="headerpanel">

		<div class="logopanel">
			<h2>
			<a href="<?php echo SITE_URL; ?>">
				<img src="<?php echo SITE_URL; ?>assets/img/logo.png" alt="Plantarum - Jardim Certo">
			</a>
			</h2>
		</div><!-- logopanel -->

		<div class="headerbar">

			<a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>

			<div class="header-right">
			<a href="<?php echo SITE_URL . 'logout'; ?>" title="Sair" class="btn btn-default btn-md mt10" id="logoutButton">Logout &nbsp; <i class="fa fa-sign-out"></i> </a>
			</div>

		</div><!-- headerbar -->

		</div><!-- header-->

	</header>

	<section>
		
		<div class="leftpanel">
		<div class="leftpanelinner">
			<div class="tab-content">
			<!-- ################# MAIN MENU ################### -->
			<div class="tab-pane active" id="mainmenu">
				<h5 class="sidebar-title">Menu Principal</h5>
				<ul class="nav nav-pills nav-stacked nav-quirk">
				<li class="nav-parent">
					<a href=""> <i class="fa fa-file-text"></i> <span>Cadastros</span></a>
					<ul class="children" style="display: block;">
					<li class='usuarios'><a href="<?php echo SITE_URL; ?>usuarios">Usuários</a></li>                
					</ul>
				</li>
				</ul>
			</div><!-- tab-pane -->
			</div><!-- tab-content -->
		</div><!-- leftpanelinner -->
		</div><!-- leftpanel -->