
<!-- Base -->
<script src="<?php echo SITE_URL; ?>assets/lib/jquery/jquery.js"></script>
<script src="<?php echo SITE_URL; ?>assets/lib/jquery-ui/jquery-ui.js"></script>
<script src="<?php echo SITE_URL; ?>assets/lib/bootstrap/js/bootstrap.js"></script>
<script src="<?php echo SITE_URL; ?>assets/js/modules/global.js"></script>

<!-- Extras -->
<script src="<?php echo SITE_URL; ?>assets/lib/jquery-toggles/toggles.js"></script>
<script src="<?php echo SITE_URL; ?>assets/lib/select2/select2.js"></script>
<script src="<?php echo SITE_URL; ?>assets/lib/jquery-mask-plugin/jquery.mask.min.js"></script>



<!-- Quirk -->
<script src="<?php echo SITE_URL; ?>assets/js/quirk.js"></script>

<script>

	GlobalUI.init();

</script>

