<?php
$params = $this->getParams();
$usuarioLogado = isset($params['usuarioLogado']) ? $params['usuarioLogado'] : null;

include_once ABSOLUTE_PATH . '/views/includes/top.php';
?>

<div class="mainpanel">

	<div class="contentpanel">

		<ol class="breadcrumb breadcrumb-quirk">
			<li class="active logopanel"><a href="#"><i class="fa fa-home mr5"></i> Home</a></li>

		</ol>
		<!-- breadcrumb -->

		<div class="panel panel-inverse row">
			<div class="panel-heading">
				<h3 class="panel-title">Home</h3>
			</div>
		</div>
		<!-- panel -->

		<div class="panel panel-default row">

			<ul class="panel-options">
				<li><a class="panel-minimize"><i class="fa fa-chevron-down"></i></a></li>
			</ul>

			<div class="panel-heading panel-minimize">
				<h4 class="panel-title">Hello World!</h4>
			</div>

			<div class="panel-body">

				<div class="form-group">
					<p>Olá!</p>
					<p>obrigado pela oportunidade de mostrar meu trabalho. Este pequeno CRUD foi desenvolvido usando um design pronto bootstrap, além de algumas soluções que eu produzi ou adaptei, como por exemplo as rotinas da pasta <b>lib</b>.</p>
					<p>Tenho 7 anos de experiencia com desenvolvimento Web, passando por linguagens como <b>Java, C++, Asp.Net, C#.Net e PHP</b>, além de criar, modelar e manter bancos de dados <b>MySQL, SQL SERVER e PostgreeSQL</b>. </p>
					<p>Espero que este pequeno exemplo MVC possa exemplificar, mesmo que superficialmente, algumas qualidades do meu código. </p>
					<p>Atenciosamente,<br>
					Caio Florio</p>
					<p>Celular<br>
					(11) 97501-4779</p>
				</div>
				<!-- formgroup --> 

			</div>
			<!-- panel body -->

		</div>
		<!-- panel -->


	</div><!-- contentpanel -->
</div><!-- mainpanel -->

</section>

<?php
include_once ABSOLUTE_PATH . '/views/includes/bottom.php';
?>


</body>
</html>