<?php
$params = $this->getParams();

$usuarioLogado = isset($params['usuarioLogado']) ? $params['usuarioLogado'] : null;

$usuario = isset($params['usuario']) ? $params['usuario'] : null;
$msg = isset($params['msg']) ? $params['msg'] : null;
$sucesso = isset($params['sucesso']) ? $params['sucesso'] : null;

include_once ABSOLUTE_PATH . '/views/includes/top.php';
?>

<div class="mainpanel">

	<div class="contentpanel">

	<ol class="breadcrumb breadcrumb-quirk">
		<li><a href="<?php echo SITE_URL; ?>"><i class="fa fa-home mr5"></i> Home</a></li>
		<li>Cadastros</li>
		<li class="active">Usuários</li>
	</ol>
	<!-- breadcrumb -->

	<div class="panel panel-inverse clearfix" style="background: none;">

		<div class="panel-heading col-sm-10">
		<h3 class="panel-title">Cadastros > Usuários</h3>
		</div>

		<div class="col-sm-2 pd0">
		<a href="#" class="btn btn-lg btn-quirk btn-block btn-default btn-voltar">VOLTAR</a>
		</div>

	</div>
	<!-- panel -->      

	<?php if (!DataValidator::isEmpty($msg)) { ?>
		<div class="clear"></div>
		<div class="row">
			<div class="col-sm-12">
			<div class="alert alert-danger">
			<?php echo $msg; ?>
			</div>
			</div>
		</div>
		<!-- row -->

	<?php } elseif ($sucesso) { ?>
		<div class="clear"></div>
		<div class="row">
			<div class="col-sm-12">
			<div class="alert alert-success">
			<?php echo "Informações salvas!"; ?>
			</div>
			</div>
		</div>
	<?php } ?>  

	<div class="clear"></div>

	<!-- Nav tabs -->
	<ul class="nav nav-tabs mt20">
		<li class="active"><a href="#dados-gerais" data-toggle="tab">Dados Gerais</a></li>
	</ul>

	<div class="tab-content">      

		<div class="tab-pane active" id="dados-gerais">

		<div class="panel row mb0">

			<div class="panel-body">

			<form action="" class="" method="post" id="voltar">
				<input type="hidden" name="controle" value="Usuario">
				<input type="hidden" name="acao" value="busca">
			</form>

			<form action="" class="form-horizontal" method="post">
				<input type="hidden" name="controle" value="Usuario">
				<input type="hidden" name="acao" value="salvar">
				<input type="hidden" name="id_usuario" value="<?php echo!DataValidator::isEmpty($usuario) ? $usuario->getId() : null ?>">

				<div class="form-group">
				<label class="col-sm-2 control-label">Status</label>
				<div class="col-sm-3">
					<select class="select2" name="status" style="width: 100%;" data-placeholder="Selecione">


					<option value="A" <?php echo!DataValidator::isEmpty($usuario) && $usuario->getStatus() == 'A' ? " selected" : "";					?>>Ativo</option>

					<option value="I" <?php echo!DataValidator::isEmpty($usuario) && $usuario->getStatus() == 'I' ? " selected" : "" ?>>Inativo</option>
					</select>
				</div>
				</div>
				<!-- formgroup -->

				<div class="form-group">
				<label class="col-sm-2 control-label">Nome do Usuário</label>          
				<div class="col-sm-7">
					<input type="text" name="nome" value="<?php echo!DataValidator::isEmpty($usuario) && !DataValidator::isEmpty($usuario->getNome()) ? $usuario->getNome() : "" ?>" class="form-control">
				</div>
				</div> 
				<!-- formgroup -->

				<div class="form-group">
				<label class="col-sm-2 control-label">E-mail</label>          
				<div class="col-sm-7">
					<input type="text" name="email" class="form-control" value="<?php echo!DataValidator::isEmpty($usuario) && !DataValidator::isEmpty($usuario->getEmail()) ? $usuario->getEmail() : "" ?>">
				</div>
				</div>
				<!-- formgroup -->   

				<div class="form-group">
				<label class="col-sm-2 control-label">Senha</label>          
				<div class="col-sm-3">
					<input type="password" name="senha" class="form-control">
				</div>
				</div>
				<!-- formgroup -->

				<hr>
				<div class="form-group mb0">
				<div class="col-sm-12">
					<button class="btn btn-success btn-quirk btn-lg fr">Gravar</button>
				</div>
				</div>
				<!-- formgroup -->

			</form>
			<!-- form -->
			</div>
			<!-- panel body -->
		</div><!-- panel -->
		</div>
		<!-- tab pane -->
	</div>
	<!-- tab content -->      
	</div><!-- contentpanel -->
</div><!-- mainpanel -->

</section>

<?php
include_once ABSOLUTE_PATH . '/views/includes/bottom.php';
?>

<script>
	$(".btn-voltar").click(function (e) {
	e.preventDefault();
	$('#voltar').submit();
	});
</script>

</body>
</html>