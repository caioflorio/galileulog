<?php
$params = $this->getParams();

$usuarios = isset($params['usuarios']) ? $params['usuarios'] : null;
$msg = isset($params['msg']) ? $params['msg'] : null;
$usuarioLogado = isset($params['usuarioLogado']) ? $params['usuarioLogado'] : null;

include_once ABSOLUTE_PATH . '/views/includes/top.php';
?>

<div class="mainpanel">

	<div class="contentpanel">

		<ol class="breadcrumb breadcrumb-quirk">
			<li><a href="<?php echo SITE_URL; ?>"><i class="fa fa-home mr5"></i> Home</a></li>
			<li>Cadastros</li>
			<li class="active">Usuários</li>
		</ol>
		<!-- breadcrumb -->

		<div class="panel panel-inverse row">
			<div class="panel-heading">
				<h3 class="panel-title">Cadastros > Usuários</h3>
			</div>
		</div>
		<!-- panel -->


		<div class="panel panel-default row">

			<ul class="panel-options">
				<li><a class="panel-minimize"><i class="fa fa-chevron-down"></i></a></li>
			</ul>

			<div class="panel-heading panel-minimize">
				<h4 class="panel-title">Busca</h4>
			</div>

			<div class="panel-body">

				<form action="" class="form-busca form-horizontal" method="post" name='busca-form'>
					<input type="hidden" name="controle" value="Usuario">
					<input type="hidden" name="acao" value="busca">

					<div class="form-group">
						<label class="col-sm-2 control-label">Status</label>
						<div class="col-sm-3">
							<select class="select2" name="status" style="width: 100%;" data-placeholder="Selecione">
								<option value="">&nbsp;</option>
								<option value="A" <?php echo @!DataValidator::isEmpty($_POST['status']) && $_POST['status'] == 'A' ? " selected" : "" ?>>Ativo</option>
								<option value="I" <?php echo @!DataValidator::isEmpty($_POST['status']) && $_POST['status'] == 'I' ? " selected" : "" ?>>Inativo</option>
								<option value="0" <?php echo @DataValidator::isEmpty($_POST['status']) ? " selected" : "" ?>>Todos</option>
							</select>
						</div>
					</div>
					<!-- formgroup --> 

					<div class="form-group">
						<label class="col-sm-2 control-label">E-mail</label>          
						<div class="col-sm-6">
							<input type="text" name="email" class="form-control" value="<?php echo @$_POST['email']; ?>">
						</div>
					</div> 
					<!-- formgroup -->  

					<hr>

					<div class="form-group">
						<div class="col-sm-12">
							<div class="fr">
								<input type="reset" class="btn btn-quirk btn-wide btn-default mr5 limpar" value="Limpar">
								<input type="submit" class="btn btn-wide btn-quirk" value="Buscar">
							</div>
						</div>
					</div>
					<!-- formgroup -->

				</form>
				<!-- form -->

			</div>
			<!-- panel body -->

		</div>
		<!-- panel -->

		<div class="panel row">
			<div class="panel-body">

				<div class="btn-group clear fr mb20">
					<a href="<?php echo SITE_URL; ?>usuarios/gerenciar/" class="btn btn-lg btn-success" >Novo Usuário</a>
				</div>
				<!-- btn group -->

				<div class="clear"></div> 

				<div class="table-responsive">

					<table class="table table-striped table-hover nomargin">

						<thead>
							<tr>
								<th>Usuário</th>
								<th>E-mail</th>
								<th width="18%">Status</th>
							</tr>
						</thead>

						<tbody>

							<?php
							if (DataValidator::isEmpty($usuarios) || $usuarios['total_linhas'] < 1) {
								?>
								<tr>
									<td colspan="4" style="text-align: center">Sua busca não retornou nenhum resultado.</td>
								</tr>
								<?php
							} else {
								foreach ($usuarios['usuarios'] as $usuario) {
									?>
									<tr>
										<td class="gerenciar_usuario" data-id="<?php echo $usuario->getId(); ?>"><a href="#" title="<?php echo $usuario->getNome(); ?>"><?php echo $usuario->getNome(); ?></a></td>
										<td class="gerenciar_usuario" data-id="<?php echo $usuario->getId(); ?>"><a href="#" title="<?php echo $usuario->getEmail(); ?>"><?php echo $usuario->getEmail(); ?></a></td>
										<td class="gerenciar_usuario" data-id="<?php echo $usuario->getId(); ?>"><a href="#" title="<?php echo $usuario->getStatusDesc(); ?>"><?php echo $usuario->getStatusDesc(); ?></a></td>

									</tr>
									<?php
								}
							}
							?>
						</tbody>

					</table>
				</div><!-- table-responsive -->
			</div>
		</div><!-- panel -->
	</div><!-- contentpanel -->
</div><!-- mainpanel -->

</section>

<?php
include_once ABSOLUTE_PATH . '/views/includes/bottom.php';
?>

<script>
	$('.gerenciar_usuario').bind('click', function (e) {
		e.preventDefault();
		location.href = '<?php echo SITE_URL; ?>usuarios/gerenciar/' + $(this).attr('data-id');
	});

	$('.limpar').click(function (e) {
		e.preventDefault();
		resetForm($('form[name=busca-form]'));

	});

	function resetForm($form) {
		$form.find('input:text, input:password, input:file, select, textarea').val('');
		$form.find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
		$form.find('select').val('').change();
	}
</script>

</body>
</html>