<?php
$params = $this->getParams();

$usuario_logado = isset($params['usuario_logado']) 	? $params['usuario_logado'] : null;
$msg 			= isset($params['msg']) 			? $params['msg'] 			: null;
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<!--<link rel="shortcut icon" href="assets/img/favicon.png" type="image/png">-->

	<title>GalileuLog - Login</title>

	<link rel="stylesheet" href="assets/lib/fontawesome/css/font-awesome.css">
	<link rel="stylesheet" href="assets/lib/jquery-toggles/toggles-full.css">
	<link rel="stylesheet" href="assets/lib/select2/select2.css">

	<link rel="stylesheet" href="assets/css/quirk.css">
	<link rel="stylesheet" href="assets/css/estilo.css">

	<script src="assets/lib/modernizr/modernizr.js"></script>

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="assets/lib/html5shiv/html5shiv.js"></script>
		<script src="assets/lib/respond/respond.src.js"></script>
	<![endif]-->
</head>

<body class="login">

	<div class="box-login clearfix">
	
		<form action="" method="post" class="form-horizontal form-login">
			<input type="hidden" name="controle" value=Index>
			<input type="hidden" name="acao" value="entra">
			
			<?php if (!DataValidator::isEmpty($msg)) { ?>
				<div class="col-sm-4">
					&nbsp;
				</div>
				
				<div class="col-sm-8">
					<div class="alert alert-danger">
						<?php echo $msg; ?>
					</div>
				</div>
				
			<?php } ?>  

			<div class="clear">&nbsp;</div>
			
			<div class="col-sm-4">
				&nbsp;
			</div>
			
			<div class="col-sm-6">    
				
				<div class="form-group">
					<div class="col-sm-3"><label>E-mail</label></div>
					<div class="col-sm-9"><input type="text" name="email" class="form-control" value="<?php 
					echo isset($_POST['email']) && !DataValidator::isEmpty($_POST['email']) ? $_POST['email'] : ''; ?>"></div>
				</div>
		
				<div class="form-group">
					<div class="col-sm-3"><label>Senha</label></div>
					<div class="col-sm-9"><input type="password" name="senha" class="form-control"></div>
				</div>       
			</div>
		
			<div class="col-sm-2" style="height: 96px;">
				<input type="submit" class="btn btn-success send-login" value="Acessar">
			</div>
		</form>
	</div>
	<!-- box login -->

	<!-- Base -->
	<script src="assets/lib/jquery/jquery.js"></script>

	<!-- Extras -->
	<script src="assets/lib/jquery-mask-plugin/jquery.mask.min.js"></script>

	<!-- Custom -->
	<script src="assets/js/modules/masks.js"></script>

	<script>
		MasksUI.init();
	</script>

</body>
</html>
