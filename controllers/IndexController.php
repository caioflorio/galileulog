<?php

require_once(ABSOLUTE_PATH  . "/lib/View.php");
require_once(ABSOLUTE_PATH  . "/lib/DataValidator.php");
require_once(ABSOLUTE_PATH  . "/controllers/LoginController.php");
require_once(ABSOLUTE_PATH  . "/controllers/UsuarioController.php");

/**
 * Controller que maneja login, logout e redirect da home
 * Pode ser adaptado para qualquer sistema.
 */
class IndexController extends LoginController {

	/**
	 * Chamado quando a url esta em branco. 
	 * Verifica se o usuario esta logado. 
	 *	    - Se sim, redireciona para a home. 
	 *	    - Se não, destrói a sessão e redireciona para a pagina de login
	 */
	public static function indexAction() {
		
		if ( DataValidator::isEmpty( session_id() )) { session_start(); }
		
		if( !isset($_SESSION[self::USUARIO]) ||
			is_null($_SESSION[self::USUARIO]) || 
			DataValidator::isEmpty($_SESSION[self::USUARIO]) 
			) {
			session_destroy();
			unset($_SESSION);
			$view = new View('views/login.php');
		} else {
			$view = new View('views/home.php');
			$view->setParams(array(
				'usuarioLogado'=>self::getUsuario()
			));
		}
		$view->showContents();
	}

	public static function loginAction() {
		
		if (DataValidator::isEmpty(session_id())) {
			session_start();
			unset($_SESSION);
			session_destroy();

			self::setUsuario(new Usuario());
			
			$view = new View('views/login.php');
			$view->showContents();
			
		} else {
			$view = new View('views/home.php');
			$view->showContents();
		}
	}

	public static function homeAction() {

		if (DataValidator::isEmpty(session_id())) { session_start(); }

		$view = new View('views/home.php');
		$view->setParams(array(
			'usuarioLogado'=>self::getUsuario()
		));
		$view->showContents();
	}

	// public static function logoutAction() {

	// 	if (DataValidator::isEmpty(session_id())) { session_start(); }

	// 	unset($_SESSION);
	// 	session_destroy();

	// 	Application::redirect(self::getHome());
	// }

	public static function validaLogin() {
		if (DataValidator::isEmpty(session_id())) session_start();
		
		self::validaSessao();
	}
	
	public static function entraAction() {
		
		if (DataValidator::isEmpty(session_id())) { session_start(); }
		
		$msg = null;
		
		$email = isset($_POST['email']) && !DataValidator::isEmpty($_POST['email']) ? $_POST['email'] : null;
		$senha = isset($_POST['senha']) && !DataValidator::isEmpty($_POST['senha']) ? $_POST['senha'] : null;
		
		try {
			
			$usuario = new Usuario();
			$usuario->setEmail($email);
			$usuario->setSenha($senha);
			
			self::setUsuario(UsuarioModel::login($usuario));
			
			$user = new Usuario();
			$user->setId(!DataValidator::isEmpty(self::getUsuario()) ? self::getUsuario()->getId() : 0 );
			
		} catch (Exception $e) {
			self::setUsuario(null);
			$msg = $e->getMessage();
		}
		
		if (!DataValidator::isEmpty($msg)) {
			$view = new View('views/login.php');
		} else {
			$view = new View('views/home.php');
		}

		$view->setParams(array('msg' => $msg,
			'usuarioLogado' => self::getUsuario(),
			));
		$view->showContents();
	}

}
