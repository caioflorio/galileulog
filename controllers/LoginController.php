<?php

require_once( ABSOLUTE_PATH . '/lib/View.php');
require_once( ABSOLUTE_PATH . '/lib/DataValidator.php');
require_once( ABSOLUTE_PATH . '/lib/Application.php');
require_once ABSOLUTE_PATH . '/models/UsuarioModel.php';
require_once ABSOLUTE_PATH . '/classes/Usuario.class.php';

class LoginController {

	const USUARIO = "login.usuario";

	public static function getHome() {
		return SITE_URL;
	}

	public static function getLoginPage() {
		return SITE_URL . 'login';
	}

	public static function getUsuario() {
		return isset($_SESSION[self::USUARIO]) ? $_SESSION[self::USUARIO] : null;
	}

	public static function setUsuario($usuario) {
		$_SESSION[self::USUARIO] = $usuario instanceof Usuario ? $usuario : null;
	}

	public static function validaSessao() {

		if (!isset($_SESSION[self::USUARIO]) ||
			is_null($_SESSION[self::USUARIO]) ||
			DataValidator::isEmpty($_SESSION[self::USUARIO]) ) {

			session_destroy();
			unset($_SESSION);

			Application::redirect(self::getHome());
		}
	}

	public static function loginAction() {

		session_start();
		unset($_SESSION);
		session_destroy();

		$view = new View('views/login.php');
		$view->showContents();
	}

	public static function logoutAction() {
		session_start();
		unset($_SESSION[self::USUARIO]);
		session_destroy();
		Application::redirect(self::getHome());
	}

}