<?php

require_once ABSOLUTE_PATH . '/controllers/LoginController.php';
require_once ABSOLUTE_PATH . '/models/UsuarioModel.php';
require_once ABSOLUTE_PATH . '/classes/Usuario.class.php';

class UsuarioController extends LoginController {
	
	function __construct() {
		if (DataValidator::isEmpty ( session_id () )) {
			session_start ();
		}
		
		self::validaSessao ();
	}

	public function buscaAction() {
		
		$msg = null;
		
		$usuarioLogado = self::getUsuario();
		
		$status = isset($_REQUEST['status']) 	&& !DataValidator::isEmpty($_REQUEST['status']) 	? $_REQUEST['status'] 	: null;
		$email = isset($_REQUEST['email']) 	&& !DataValidator::isEmpty($_REQUEST['email']) 	? $_REQUEST['email'] 	: null;
		
		$usuariosArr = array();
		$usuariosArr = UsuarioModel::lista(null, null, $email, $status);
		
		$view = new View('views/usuario/lista.php');
		$view->setParams(array(
				'msg' => $msg,
				'usuarios' => $usuariosArr,
				'usuarioLogado' => $usuarioLogado
		));
		$view->showContents();
	}
	
	public function gerenciarAction() {
		
		$msg = null;
		
		$usuarioLogado = self::getUsuario();
		
		$id_edita = isset($_REQUEST['id_edita']) && !DataValidator::isEmpty($_REQUEST['id_edita']) ? $_REQUEST['id_edita'] : null;
		
		if(!DataValidator::isEmpty($id_edita)) {
			$usuario = UsuarioModel::getById($id_edita);
		} else {
			$usuario = new Usuario();
		}
		
		$view = new View('views/usuario/gerenciar.php');
		$view->setParams(array(
				'msg' => $msg,
				'usuario' => $usuario,
				'usuarioLogado' => $usuarioLogado
		));
		$view->showContents();
	}
	
	public function salvarAction() {
		
		$msg = "";
		
		$usuarioLogado = self::getUsuario();
		
		//filtros de verificacao de campos vazios
		if(!isset($_REQUEST['status']) || DataValidator::isEmpty($_REQUEST['status'])) {
			$msg = "Status é obrigatório";
		}
		if(!isset($_REQUEST['nome']) || DataValidator::isEmpty($_REQUEST['nome'])) {
			$msg = "Nome é obrigatório";
		}
		if(!isset($_REQUEST['email']) || DataValidator::isEmpty($_REQUEST['email'])) {
			$msg = "E-mail é obrigatório";
		}
		//se o usuário estiver sendo alterado, a senha não é obrigatoria
		if((!isset($_REQUEST['id_usuario']) || DataValidator::isEmpty($_REQUEST['id_usuario'])) && (!isset($_REQUEST['senha']) || DataValidator::isEmpty($_REQUEST['senha']))) {
			$msg = "Senha é obrigatória";
		}
		
		$sucesso = false;
		
		$usuario = new Usuario();
		
		$usuario->setId(isset($_REQUEST['id_usuario']) && !DataValidator::isEmpty($_REQUEST['id_usuario']) ? $_REQUEST['id_usuario'] : null);
		$usuario->setNome(isset($_REQUEST['nome']) && !DataValidator::isEmpty($_REQUEST['nome']) ? $_REQUEST['nome'] : null);
		$usuario->setEmail(isset($_REQUEST['email']) && !DataValidator::isEmpty($_REQUEST['email']) ? $_REQUEST['email'] : null);
		$usuario->setSenha(isset($_REQUEST['senha']) && !DataValidator::isEmpty($_REQUEST['senha']) ? md5($_REQUEST['senha']) : null);
		$usuario->setStatus(isset($_REQUEST['status']) && !DataValidator::isEmpty($_REQUEST['status']) ? $_REQUEST['status'] : null);
		
		if(DataValidator::isEmpty($msg)) {
			
			$retorno = UsuarioModel::salvar($usuario);
			
			if($retorno)
				$sucesso = true;
			else 
				$msg = "Erro na gravação!";
		}
		
		$view = new View('views/usuario/gerenciar.php');
		$view->setParams(array(
				'msg' => $msg,
				'usuarioLogado' => $usuarioLogado,
				'sucesso' => $sucesso,
				'usuario' => $usuario
		));
		$view->showContents();
	}
}